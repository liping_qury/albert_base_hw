#hw_2020/first_domain/origin_0720_merge_data


export DATA_DIR=./dataset
export BERT_BASE_DIR=/home/qury/Desktop/trainQueryCLS/modelParas/uncased_L-12_H-768_A-12
export TRAINED_CLASSIFIER=./output/model
export OUTPUT_DIR=./result

python3 /home/qury/Desktop/trainQueryCLS/model/multi_task_small_bert/small_bert/bert-master/run_classifier.py \
  --task_name=Query2Domain_HW_CLS \
  --do_predict=true \
  --data_dir=$DATA_DIR \
  --vocab_file=$BERT_BASE_DIR/vocab.txt \
  --bert_config_file=$BERT_BASE_DIR/bert_config.json \
  --init_checkpoint=$TRAINED_CLASSIFIER \
  --max_seq_length=128 \
  --output_dir=$OUTPUT_DIR
