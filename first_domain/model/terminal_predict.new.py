"""
19.6.24
good version for saved model
predict based terminal
"""
import tensorflow as tf
import numpy as np
import json
import codecs
import pickle
import os
from datetime import datetime
from models import create_model, InputFeatures, get_assignment_map_from_checkpoint
import tokenization, modeling
from args_parser import get_args_parser
args = get_args_parser()

pb_model_dir_small = "./saved_model/1"
bert_dir = "./"

def convert_single_example(ex_index, example, label_list, max_seq_length,
                           tokenizer, mode):
    """
    Converts a single `InputExample` into a single `InputFeatures`.
    :param ex_index: index
    :param example: 一个样本
    :param label_list: 标签列表
    :param max_seq_length:
    :param tokenizer:
    """

    label_map = {}
    
    # 1表示从1开始对label进行index化
    for (i, label) in enumerate(label_list, 1):
        label_map[label] = i

    tokens = example
    # tokens = tokenizer.tokenize(example.text)
    # 序列截断
    if len(tokens) >= max_seq_length - 1:
        tokens = tokens[0:(max_seq_length - 2)]  # -2 的原因是因为序列需要加一个句首和句尾标志
    ntokens = []
    segment_ids = []
    # label_ids = []

    label_id = label_map["else"]    
    ntokens.append("[CLS]")  # 句子开始设置CLS 标志
    segment_ids.append(0)
    # append("O") or append("[CLS]") not sure!
    # label_ids.append(label_map["[CLS]"])  # O OR CLS 没有任何影响，不过我觉得O 会减少标签个数,不过句首和句尾使用不同的标志来标注，使用LCS 也没毛病
    for i, token in enumerate(tokens):
        ntokens.append(token)
        segment_ids.append(0)
        # label_ids.append(0)
    ntokens.append("[SEP]")  # 句尾添加[SEP] 标志
    segment_ids.append(0)
    # append("O") or append("[SEP]") not sure!
    # label_ids.append(label_map["[SEP]"])
    input_ids = tokenizer.convert_tokens_to_ids(ntokens)  # 将序列中的字(ntokens)转化为ID形式
    input_mask = [1] * len(input_ids)

    # padding, 使用
    while len(input_ids) < max_seq_length:
        input_ids.append(0)
        input_mask.append(0)
        segment_ids.append(0)

    assert len(input_ids) == max_seq_length
    assert len(input_mask) == max_seq_length
    assert len(segment_ids) == max_seq_length

    # 结构化为一个类
    feature = InputFeatures(
        input_ids=input_ids,
        input_mask=input_mask,
        segment_ids=segment_ids,
        label_ids=label_id,
    )
    return feature

def convert_id_to_label(probabilities, idx2label):
    """
    将id形式的结果转化为真实序列结果
    :param probabilities:
    :param idx2label:
    :return:
    """
    result = []
    for row in range(batch_size):
        curr_seq = []
        for ids in probabilities[row][0]:
            if ids == 0:
                break
            curr_label = idx2label[ids]
            curr_seq.append(curr_label)
        result.append(curr_seq)
    return result

def convert(line):
    feature = convert_single_example(0, line, label_list, args.max_seq_length, tokenizer, 'p')
    input_ids = np.reshape([feature.input_ids],(batch_size, args.max_seq_length))
    input_mask = np.reshape([feature.input_mask],(batch_size, args.max_seq_length))
    segment_ids = np.reshape([feature.segment_ids],(batch_size, args.max_seq_length))
    return input_ids, input_mask, segment_ids

label_list = ['news', 'shopping', 'life', 'else']
label2id = {label:idx for (idx,label) in enumerate(label_list, 1)}
id2label = {value: key for key, value in label2id.items()}

num_labels = len(label_list)

is_training=False
use_one_hot_embeddings=False
batch_size=1

tokenizer = tokenization.FullTokenizer(
    vocab_file=os.path.join(bert_dir, '/home/qury/Desktop/trainQueryCLS/modelParas/uncased_L-12_H-768_A-12/vocab.txt'), do_lower_case=args.do_lower_case)
"""
with tf.Session(graph=tf.Graph()) as sess:
    tf.saved_model.loader.load(sess, ["serve"], pb_model_dir_small)
    graph = tf.get_default_graph()

    input_ids_p = tf.get_default_graph().get_tensor_by_name("input_ids:0")
    input_mask_p = tf.get_default_graph().get_tensor_by_name("input_mask:0")
    probabilities = tf.get_default_graph().get_tensor_by_name("pred_prob:0")

    while True:
        print('input the test query:')
        query = str(input())
        start = datetime.now()
        query = tokenizer.tokenize(query)
        input_ids, input_mask, segment_ids = convert(query)
        feed_dict = {input_ids_p: input_ids,
                        input_mask_p: input_mask}
        # run session get current feed_dict result
        probabilities_result = sess.run([probabilities], feed_dict)
        # pred_label_result = convert_id_to_label(probabilities_result, id2label)
        print(probabilities_result)
        #todo: 组合策略
        # result = strage_combined_link_org_loc(query, pred_label_result[0])
        print('time used: {} sec'.format((datetime.now() - start).total_seconds()))
"""

with tf.Session(graph=tf.Graph()) as sess:
    tf.saved_model.loader.load(sess, ["serve"], pb_model_dir_small)
    graph = tf.get_default_graph()

    input_ids_p = tf.get_default_graph().get_tensor_by_name("input_ids:0")
    input_mask_p = tf.get_default_graph().get_tensor_by_name("input_mask:0")
    probabilities = tf.get_default_graph().get_tensor_by_name("pred_prob:0")
    
    with open('query_test.dat', 'r') as f, open('query_test_out.dat', 'w') as out:
        line = f.readline()
        while(line):
            dic = {}
            query = str(line.strip())
            query = tokenizer.tokenize(query)
            input_ids, input_mask, segment_ids = convert(query)
            dic['input_ids'] = input_ids[0].tolist()
            dic['input_mask'] = input_mask[0].tolist()
            res = {'instances':[dic]}
            out.write(json.dumps(res) + '\n')
            line = f.readline()






