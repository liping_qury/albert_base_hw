from __future__ import absolute_import
from __future__ import division
import sys
import collections
import csv
import os
import modeling
import optimization
import tokenization
import tensorflow as tf
import numpy as np
from sklearn import metrics
import time
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix 


#os.environ["CUDA_VISIBLE_DEVICES"] = "4"
flags = tf.flags

FLAGS = flags.FLAGS


flags.DEFINE_string(
    "bert_config_file", '/home/qury/Desktop/trainQueryCLS/modelParas/uncased_L-12_H-768_A-12/bert_config.json',
    "The config json file corresponding to the pre-trained BERT model. "
    "This specifies the model architecture.")

flags.DEFINE_string(
    "init_checkpoint", '../output/model/model.ckpt-2175',
    "Initial checkpoint (usually from a pre-trained BERT model).")

flags.DEFINE_integer(
    "max_seq_length", 128,
    "The maximum total input sequence length after WordPiece tokenization. "
    "Sequences longer than this will be truncated, and sequences shorter "
    "than this will be padded.")

def create_model(bert_config, is_training, input_ids, input_mask, segment_ids, num_labels, use_one_hot_embeddings):
  """Creates a classification model."""
  model = modeling.BertModel(
      config=bert_config,
      is_training=is_training,
      input_ids=input_ids,
      input_mask=input_mask,
      token_type_ids=segment_ids,
      use_one_hot_embeddings=use_one_hot_embeddings)

  output_layer = model.get_pooled_output()

  hidden_size = output_layer.shape[-1].value

  output_weights = tf.get_variable(
      "output_weights", [num_labels, hidden_size],
      initializer=tf.truncated_normal_initializer(stddev=0.02))
  #initializer=tf.truncated_normal_initializer(stddev=0.02)

  output_bias = tf.get_variable(
      "output_bias", [num_labels], initializer=tf.zeros_initializer())

  with tf.variable_scope("loss"):
    if is_training:
      # I.e., 0.1 dropout
      output_layer = tf.nn.dropout(output_layer, keep_prob=0.9)

    logits = tf.matmul(output_layer, output_weights, transpose_b=True)
    logits = tf.nn.bias_add(logits, output_bias)
    probabilities = tf.nn.softmax(logits, axis=-1)
    log_probs = tf.nn.log_softmax(logits, axis=-1)

    return (logits, probabilities)


def model_fn_builder(bert_config, num_labels, init_checkpoint, use_one_hot_embeddings):
  """Returns `model_fn` closure for TPUEstimator."""

  def model_fn(features, mode, params):  # pylint: disable=unused-argument
    """The `model_fn` for TPUEstimator."""

    input_ids = features["input_ids"]
    input_mask = features["input_mask"]
    segment_ids = features["segment_ids"]

    is_training = (mode == tf.estimator.ModeKeys.TRAIN)

    logits, probabilities = create_model(
        bert_config, is_training, input_ids, input_mask, segment_ids, num_labels, use_one_hot_embeddings)

    tvars = tf.trainable_variables()
    initialized_variable_names = {}
    if init_checkpoint:
      (assignment_map, initialized_variable_names
      ) = modeling.get_assignment_map_from_checkpoint(tvars, init_checkpoint)
      tf.train.init_from_checkpoint(init_checkpoint, assignment_map)

    output_spec = None
    
    predictions = {'probs':probabilities,'res':tf.argmax(probabilities)}
    #predictions = {'probs':probabilities}
    output_spec = tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)
    return output_spec

  return model_fn

def serving_input_receiver_fn():
    
    feature_spec = {
        "input_ids":
            tf.FixedLenFeature([128], tf.int64),
        "input_mask":
            tf.FixedLenFeature([128], tf.int64),
        "segment_ids":
            tf.FixedLenFeature([128], tf.int64)
    }
    serialized_tf_example = tf.placeholder(dtype = tf.string,shape = [1],name = 'default_batch_size')
    receiver_tensors = {'examples': serialized_tf_example}
    features = tf.parse_example(serialized_tf_example, feature_spec)
    return tf.estimator.export.ServingInputReceiver(features, receiver_tensors)

def main(_):

  bert_config = modeling.BertConfig.from_json_file(FLAGS.bert_config_file)

  if FLAGS.max_seq_length > bert_config.max_position_embeddings:
    raise ValueError(
        "Cannot use sequence length %d because the BERT model "
        "was only trained up to sequence length %d" %
        (FLAGS.max_seq_length, bert_config.max_position_embeddings))

  label_list = ['news', 'shopping', 'life', 'else'] 

  model_fn = model_fn_builder(
      bert_config=bert_config,
      num_labels=len(label_list),
      init_checkpoint=FLAGS.init_checkpoint,
      use_one_hot_embeddings=False)

  estimator = tf.estimator.Estimator(model_fn = model_fn,model_dir ='../output/model/')
  estimator.export_savedmodel('./estimator',serving_input_receiver_fn)

if __name__ == "__main__":
  tf.app.run()
