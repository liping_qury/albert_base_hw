# -*- coding=utf-8 -*-
"""
#
# File Name: BertPredict.py
# @Author: lidonghui02@baidu.com
# Copyright (c) 2019 Baidu.com, Inc. All Rights Reserved
#
"""
#import sys
#reload(sys)
#sys.setdefaultencoding('utf-8')
import tensorflow as tf
import os
import json
import tokenization
import time
import numpy as np
# import config
#from tornado.options import define, options
#import tornado.httpserver
#import tornado.ioloop
#import tornado.options
#import tornado.web
#define("port", default=22325, help="run on the given port", type=int)

def truncate_seq_pair(tokens_a, tokens_b, max_length):
  """Truncates a sequence pair in place to the maximum length."""
  while True:
    total_length = len(tokens_a) + len(tokens_b)
    if total_length <= max_length:
      break
    if len(tokens_a) > len(tokens_b):
      tokens_a.pop()
    else:
      tokens_b.pop()
def convert_single_example(text_a, text_b, max_seq_length, tokenizer):
  """Converts a single `InputExample` into a single `InputFeatures`."""
  tokens_a = tokenizer.tokenize(text_a)
  tokens_b = tokenizer.tokenize(text_b)  
  #print('here?')
  print(tokens_a)
  #print(tokens_b)
  #exit(-1)
  if tokens_b == '0':
    truncate_seq_pair(tokens_a, tokens_b, max_seq_length)
  else:
    #print('token a', len(tokens_a))
    #print('m s l', max_seq_length)
    if len(tokens_a) > max_seq_length:
      #print('here ha')
      tokens_a = tokens_a[:(max_seq_length-2)] # -2
  #print(tokens_a[-2])
  #print(tokens_a[-1])
  #print('here??')
  tokens = []
  segment_ids = []
  tokens.append("[CLS]")
  segment_ids.append(0)
  for token in tokens_a:
    tokens.append(token)
    segment_ids.append(0)
  tokens.append("[SEP]")
  segment_ids.append(0)
  #print(tokens[-3])
  #print(tokens[-2])
  #print(tokens[-1])
  #exit(-1)
  #if tokens_b:
  #  for token in tokens_b:
  #    tokens.append(token)
  #    segment_ids.append(1)
  #  tokens.append("[SEP]")
  #  segment_ids.append(1)
  input_ids = tokenizer.convert_tokens_to_ids(tokens)
  input_mask = [1] * len(input_ids)
  while len(input_ids) < max_seq_length:
    input_ids.append(0)
    input_mask.append(0)
    segment_ids.append(0)
  return input_ids, input_mask, segment_ids
class BertSpamModel(object):
    def __init__(self):
        # self.vocab_file = config.vocab_path # check it here
        self.vocab_file = "/home/qury/Desktop/trainQueryCLS/modelParas/uncased_L-12_H-768_A-12" # check it here
        self.tokenizer = tokenization.FullTokenizer(vocab_file = self.vocab_file, do_lower_case = True)
        self.max_seq_length = 128
        self.predict_fn = tf.contrib.predictor.from_saved_model("estimator/1596693739/")
    def create_int_feature(self,values):
        return tf.train.Feature(int64_list=tf.train.Int64List(value=list(values)))
    def create_input(self,title, comment):
        text_a = tokenization.convert_to_unicode(title)
        text_b = tokenization.convert_to_unicode(comment)
       
        input_ids, input_mask, segment_ids = convert_single_example(text_a, text_b, self.max_seq_length, self.tokenizer) 
        features = {
                'input_ids':self.create_int_feature(input_ids),
                'input_mask':self.create_int_feature(input_mask),
                'segment_ids':self.create_int_feature(segment_ids),
                }
        
        example = tf.train.Example(features = tf.train.Features(feature=features)).SerializeToString()
        examples =[example]
        
        return examples 
    def predict(self, title, comment):
        examples = self.create_input(title, comment)
        res = self.predict_fn({'examples':examples})['probs']
        # res = [float(i) for i in res]
        # res[0] = 1 - res[0]
        return res
def test1():
    bsm = BertSpamModel()
    t1 = time.time()
    title = "逝去的童年 留守的童真  祝越来越大的我们 #六一儿童节# 快乐 今天我们都是快乐的大儿童[嘻嘻]"
    title = "新疆喀什，九十年代初。"
    title = "E-sports"
    comment = "0"
    probs = bsm.predict(title,comment)
    print(probs)
    """
    with open('./data/test.tsv', 'r') as f:
        titles = f.readlines()
    with open('./check_out', 'w') as f:
        for here in titles:
            title = here.split('\t')[1]
            #print(title)
            probs = bsm.predict(title,comment)
            f.write(str(probs)+'\n')
        #exit(-1)
    """
    # for line in open(sys.argv[1]):
    #     sl = line.strip().split('\t')
    #     if len(sl) !=3:
    #         continue
    #     title,comment,label = sl[0],sl[1],sl[2]
    #     idx = comment.find('===>>>')
    #     if idx != -1:
    #         comment = comment[:idx].encode('utf-8')
    #     probs = bsm.predict(title,comment)
    #     pred = np.argmax(probs)
    #     print label,pred,probs[0],probs[1],probs[2],probs[3],probs[4],comment
    #print time.time() - t1


"""
bsm = BertSpamModel()
class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        test_case = self.get_argument('testcase')
        print(test_case)
        comment = "0"
        res_json = bsm.predict(test_case,comment)
        predict_list = list(res_json)[0]
        current_max = max(predict_list)
        current_index_max = np.argwhere(predict_list == current_max)
        #current_index_max = predict_list.index(current_max)
        if current_max >= 0.75:
            self.write(str(current_index_max[0][0]))
            print(current_index_max)
        else:
            self.write(str(0))
            print(0)
        #print(predict_list[0])
        #print(type(predict_list[0]))
        #print('res_json', res_json)
        #label = res_json.get('vulgar', 0)
        #score = res_json.get('score', 'none')
        #print(label)
        #self.write({'label':str(label), 'score':str(score)})
        #self.write(str(label))

"""

if __name__ == '__main__':
    #os.environ["CUDA_VISIBLE_DEVICES"] = "3"
    test1()
    #tornado.options.parse_command_line()
    #app = tornado.web.Application(handlers=[(r"/", IndexHandler)])
    #http_server = tornado.httpserver.HTTPServer(app)
    #http_server.listen(options.port)
    #tornado.ioloop.IOLoop.instance().start()




