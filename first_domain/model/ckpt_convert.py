"""
19.6.24
convert ckpt to saved model file
predict based terminal
"""
import tensorflow as tf
import numpy as np
import codecs
import pickle
import os
from datetime import datetime
from datetime import datetime
from models import create_model, InputFeatures, get_assignment_map_from_checkpoint
import tokenization, modeling
from args_parser import get_args_parser
from tensorflow.python.saved_model import signature_constants
from tensorflow.python.saved_model import tag_constants

model_name = 'hw_first_domain'

args = get_args_parser()

input_checkpoint = "../output/model/model.ckpt-2175"
bert_base = '/home/qury/Desktop/trainQueryCLS/modelParas/uncased_L-12_H-768_A-12'
export_dir = './saved_model'
builder = tf.saved_model.builder.SavedModelBuilder(export_dir)
label_list = ['news', 'shopping', 'life', 'else'] 
label2id = {label:idx for (idx,label) in enumerate(label_list, 1)}
id2label = {value: key for key, value in label2id.items()}

num_labels = len(label_list)

is_training=False
use_one_hot_embeddings=False
batch_size=1

graph = tf.get_default_graph()
input_ids_p, input_mask_p, label_ids_p, segment_ids_p = None, None, None, None
tokenizer = tokenization.FullTokenizer(
        vocab_file=os.path.join(bert_base, 'vocab.txt'), do_lower_case=args.do_lower_case)


sigs = {}
with graph.as_default():
    print("##########################################")
    input_ids_p = tf.placeholder(tf.int32, [None, args.max_seq_length], name="input_ids")
    input_mask_p = tf.placeholder(tf.int32, [None, args.max_seq_length], name="input_mask")
    bert_config = modeling.BertConfig.from_json_file(os.path.join(bert_base, 'bert_config.json'))
    (logits, probabilities) = create_model(
        bert_config=bert_config, is_training=False, input_ids=input_ids_p, input_mask=input_mask_p, segment_ids=None,
        labels=None, num_labels=num_labels, use_one_hot_embeddings=False)
    probabilities = tf.identity(probabilities, "pred_prob")
    with tf.Session() as sess:
        tvars = tf.trainable_variables()
        initialized_variable_names = {}
        (assignment_map, initialized_variable_names) = get_assignment_map_from_checkpoint(tvars, input_checkpoint)
        tf.train.init_from_checkpoint(input_checkpoint, assignment_map)
        sess.run(tf.global_variables_initializer())

        temp = signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY
        print("###",temp)
        print("###",tag_constants.SERVING)
        sigs[temp] = \
            tf.saved_model.signature_def_utils.predict_signature_def(
                {"input_ids": input_ids_p, "input_mask":input_mask_p}, {"probabilities": probabilities})

        builder.add_meta_graph_and_variables(sess,
                                            [tag_constants.SERVING],
                                            signature_def_map=sigs)

builder.save()
    

