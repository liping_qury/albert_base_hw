# coding=utf-8

def fun():
    with open(file_in, 'r') as f_in, open(file_out, 'w') as f_out:
        line = f_in.readline()
        while(line):
            line = line.strip().split('\t')
            m = 0.0
            m_i = 0
            for i in range(len(line)):
                if float(line[i]) > m:
                    m_i = i
                    m = float(line[i])
            f_out.write(cls[m_i] + '\t' + str(m) + '\n')
            line = f_in.readline()


if __name__ == "__main__":
    
    file_in = './result/test_results.tsv'
    #file_in = './output/model/test_results.tsv'
    file_out = './result/test_cls.dat'
    
    cls = ['news', 'shopping', 'life', 'else']
    fun()


