#hw_2020/first_domain/origin_0720
DATA_DIR=./dataset
BERT_BASE_DIR=/home/qury/Desktop/trainQueryCLS/runbertModel/hw_2020/albert/albert/albert_base
OUTPUT_DIR=./output/model
#init_checkpoint="./output/model/model.ckpt-8458"
#init_checkpoint=$BERT_BASE_DIR/bert_model.ckpt \

"""
python3 /home/qury/Desktop/trainQueryCLS/runbertModel/hw_2020/albert/albert/run_classifier.py \
  --task_name=Query2Domain_HW_CLS \
  --do_train=true \
  --do_eval=true \
  --do_predict=false \
  --data_dir=$DATA_DIR/ \
  --vocab_file=$BERT_BASE_DIR/30k-clean.vocab \
  --albert_config_file=$BERT_BASE_DIR/albert_config.json \
  --init_checkpoint=$BERT_BASE_DIR/model.ckpt-best \
  --spm_model_file=$BERT_BASE_DIR/30k-clean.model \
  --max_seq_length=128 \
  --train_batch_size=16 \
  --learning_rate=2e-5 \
  --train_step=1000 \
  --save_checkpoints_steps=200 \
  --output_dir=$OUTPUT_DIR \
  --do_lower_case 
"""
python3 /home/qury/Desktop/trainQueryCLS/runbertModel/hw_2020/albert/albert/run_classifier.py \
  --data_dir=$DATA_DIR/ \
  --output_dir=$OUTPUT_DIR \
  --init_checkpoint=$BERT_BASE_DIR/model.ckpt-best \
  --albert_config_file=$BERT_BASE_DIR/albert_config.json \
  --spm_model_file=$BERT_BASE_DIR/30k-clean.model \
  --do_train \
  --do_eval \
  --use_spm \
  --do_lower_case \
  --max_seq_length=128 \
  --optimizer=adamw \
  --task_name=Query2Domain_HW_CLS \
  --learning_rate=3e-5 \
  --train_step=1000 \
  --save_checkpoints_steps=200 \
  --train_batch_size=16


