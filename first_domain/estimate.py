
label_list = ['news', 'shopping', 'life', 'else'] 
test_file = './dataset/test.tsv'
predict_file = './result/test_results.tsv'
save_file = './result/estimate.txt'

data_list = []
with open(test_file, 'r') as test_f, open(predict_file, 'r') as predict_f, open(save_file, 'w') as save_f:
    test_lines = test_f.readlines()
    predict_lines = predict_f.readlines()
    # print(test_lines)
    # print(predict_lines)
    # ['subito', 'Shopping']
    # ['0.27437443', '0.24323684', '0.20065534', '0.25794232', '0.023791112']
    for i, k in enumerate(test_lines):
        tt_lines = k.strip().split('\t')
        pp_lines = predict_lines[i].split()
        pp_result = [label_list[x] + ',' + y for x, y in enumerate(pp_lines)]
        # print(pp_result)
        tt_lines.extend(pp_result)
        # data_list.append(tt_lines)
        print(','.join(tt_lines))
        save_f.write(','.join(tt_lines) + "\n")

# print(data_list)

